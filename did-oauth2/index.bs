<pre class='metadata'>
Title: did:oauth2 Decentralized Identifier Method Specification
Boilerplate: issues-index no, copyright no, abstract no
Shortname: did-oauth2-spec
Level: 1
Status: w3c/ED
Group: Credentials Community Group
URL: https://github.com/w3c-ccg/did-method-oauth2
Repository: https://github.com/w3c-ccg/did-method-oauth2
Max ToC Depth: 2
Editor: Wayne Chang, ConsenSys https://consensys.net, wayne.chang@consensys.net
!Authors: Wayne Chang, ConsenSys https://consensys.net, wayne.chang@consensys.net
Abstract: A DID method specification based on OAuth 2.0
</pre>

## Copyright
<p class="copyright">
<a href="https://www.w3.org/Consortium/Legal/ipr-notice#Copyright">Copyright</a>
© 2019 the Contributors to the DID Web Method Specification, published by the
<a href="https://www.w3.org/community/credentials/">Credentials Community
Group</a> under the <a href="https://www.w3.org/community/about/agreements/cla/">W3C Community Contributor License Agreement (CLA)</a>.
A human-readable <a href="https://www.w3.org/community/about/agreements/cla-deed/">summary</a>
is available.
</p>

# `did:oauth2` Method Specification

## Preface

The OAuth 2.0 DID method specification conforms to the requirements specified
in the [DID specification](https://w3c-ccg.github.io/did-spec/), currently
published by the W3C Credentials Community Group. For more information about
DIDs and DID method specifications, please see the [DID
Primer](https://github.com/WebOfTrustInfo/rebooting-the-web-of-trust-fall2017/blob/master/topics-and-advance-readings/did-primer.md)

## Abstract

OAuth 2.0 adoption is widespread. Most Internet users already have several
OAuth 2.0-enabled accounts. The DID specification states that "methods may also
be developed for identifiers registered in federated or centralized identity
management systems." This is one such method. It aims to facilitate DID
adoption and therefore user control by leveraging network effects of the most
prominent networks today. Although the DIDs produced are likely to be
controlled by a small number of large entities, they can still unlock several
forms of new decentralization such as user-held verifiable credentials, which
may be kept private and conceivably used to bridge to alternate user-controlled
DIDs. When enabled across billions of existing accounts, there is potential for
substantial impact.

## Example

```json
{
  "@context": "https://w3id.org/did/v1",
  "id": "did:oauth2:accounts.google.com:john.doe@gmail.com",
}
```

## Target System

The target system of the OAuth2 DID method is the user device that
authenticates its user and authorizes resources through an OAuth 2.0 issuer.

## DID Method Name

The namestring that shall identify this DID method is: `oauth2`

A DID that uses this method MUST begin with the following prefix: `did:oauth2`.
Per the DID specification, this string MUST be in lowercase. The remainder of
the DID, after the prefix, is specified below.

## Method Specific Identifier

The method specific identifier is composed of an OAuth 2.0 issuer identifier
and a username, separated by a hash mark. The issuer identifier is described in
[RFC8414](https://tools.ietf.org/html/rfc8414), and in general is a URL that
uses the "https" scheme and has no query or fragment components. The username
SHOULD belong to one of the categories specified in the memo [Username
Interoperability](https://tools.ietf.org/id/draft-saintandre-username-interop-01.html).


	oauth2-did = "did:web:" issuer-identifier ":" username

The hash mark (#) is chosen as the separator because the issuer identifier MUST
NOT contain URL fragment components. The username is specified after the issuer
identifier as it may contain hash marks (#) itself.

### Example

```
did:oauth2:accounts.google.com#john.doe@gmail.com
```

## JSON-LD Context Definition

The definition of the OAuth2 DID JSON-LD context is:

    {
        "@context":
        {}
    }

It does not add any additional fields to the DID Document as there is no need
for cryptographic verification.

## CRUD Operation Definitions

### Create (Register)

Creating a DID is done by
1. Creating an account in a service that supports OAuth 2.0.

For the issuer `login.microsoftonline.com` and username `jane.smith@mycorp.com`, the DID will be:

```
did:oauth2:login.microsoftonline.com#jane.smith@mycorp.com
```

### Read (Resolve)

The following steps must be executed to resolve the DID document from an OAuth2
DID:

1. Parse the issuer identifier and username identifier.
1. Register with the issuer as described in [RFC7591](https://tools.ietf.org/html/rfc7591). The issuer may have documentation and tools to assist with this process. The issuer MUST support a method to verify the account's username, such as the [OpenID Connect 1.0 ID Token](https://openid.net/specs/openid-connect-core-1_0.html#IDToken) with an `email` claim.

    a. (OpenID Connect Discovery 1.0) For services that support [OpenID Connect Discovery 1.0](https://openid.net/specs/openid-connect-discovery-1_0.html), additional verification for the issuer identifier is possible. Verify that the `/.well-known/openid-configuration` contains the top level `issuer` field and that this value corresponds to the issuer identifier, allowing for an optional prefix of `https://`.

1. With the appropriate scope set to allow username verification, obtain an OAuth 2.0 Authorization Grant, as described in [RFC6749](https://tools.ietf.org/html/rfc6749#section-4).

    a. (OpenID Connect 1.0 ID Token and Email) For services that (1) use email as the username and (2) support the OpenID Connect 1.0 ID Token, request the `email` claim during authorization, validate the JWT, and use the `email` value to verify the DID.

    a. (OAuth 2.0-only) For OAuth 2.0-only services (for example, Facebook in September 2019), the documentation must be consulted to determine a secure method for username verification. This is typically done through username embedding in authorization responses or a URL that may be accessed with the Access Token.

1. After the optional verification of the issuer identifier and required verification of the username, a DID Document is constructed with the identifier as the only field aside from the required `@context`.

For example, the DID

```
did:oauth2:accounts.google.com#john.doe@gmail.com
```

becomes the DID Document

```
{
    "@context": "https://w3id.org/did/v1",
    "id": "did:oauth2:accounts.google.com#john.doe@gmail.com"
}
```


### Update

The DID Document cannot be updated for an identifier as there is nothing to
update.

### Delete (Revoke)

The DID Document is deleted through the issuer by preventing the OAuth 2.0
authorization process from completing.

## Security and Privacy Considerations

### Centralization Risk
The issuer controls all aspects of CRUD. They can create, update, and delete
accounts at will or erroneously.

### PII Risk
These DIDs are likely to contain information that can be identify a person or
organization, so they SHOULD be kept _private_. For example, the DID

```
did:oauth2:login.microsoftonline.com#james.smith@widgetco.com
```

may help an attacker with hints that:
- This DID may refer to James Smith
- James Smith may work at widgetco.com
- widgetco.com may have Microsoft Online accounts

### Username Collisions
For some OAuth 2.0 providers, the usernames `jane.smith@mycorp.com` and
`janesmith@mycorp.com` may resolve to the same account. This should be taken
into consideration in security and trust models.

## Reference Implementations

N/A
