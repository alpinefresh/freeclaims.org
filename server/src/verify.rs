use maplit::btreemap;
use rand::{thread_rng, Rng, seq::SliceRandom};
use rand::distributions::Alphanumeric;
use uuid::Uuid;
use std::error::Error;
use std::fmt;
use std::env;
use reqwest::{Client, header};
use crate::credential;

use std::str;
use std::net::Ipv4Addr;
use std::str::FromStr;
use trust_dns::op::DnsResponse;
use trust_dns::rr::{DNSClass, Name, RData, Record, RecordType};
use trust_dns::client::{Client as DNSClient, ClientConnection as DNSClientConnection, ClientStreamHandle as DNSClientStreamHandle, SyncClient as DNSSyncClient};
use credential::{Credential, Proof, Subject};
use trust_dns::udp::UdpClientConnection;
use std::collections::{BTreeMap, HashMap as Map};

use serde::{Serialize, Deserialize};
use serde_json::{json, Value, to_string_pretty};
use chrono::prelude::*;

type Result<T> = std::result::Result<T, Box<dyn Error>>;

#[derive(Debug)]
struct VerificationError {
    message: String,
}

impl Error for VerificationError {}

impl fmt::Display for VerificationError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

fn error<T>(message: String) -> Result<T> {
    Err(Box::new(VerificationError {message: message}))
}


#[derive(Serialize, Deserialize, Debug)]
pub struct Verification {
    id: Uuid,
    created_time: DateTime<Utc>,
    type_: Type,
    challenge: Option<String>,
    response: Option<String>,
    evidence: Vec<(DateTime<Utc>, String)>,
    is_complete: bool,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "lowercase")]
pub enum Type {
    Twilio { phone_number: String },
    Mailgun { email: String },
    Dns { domain: String },
    OAuth2 { issuer: String, username: String },
}

fn random_6digit_code() -> String {
    let nums = [0,1,2,3,4,5,6,7,8,9];
    let mut rng = thread_rng();

    nums.choose_multiple(&mut rng, 6)
        .map(|d| std::char::from_digit(*d, 10).unwrap())
        .collect()
}

fn random_string_upper(nchar: usize) -> String {
    thread_rng()
        .sample_iter(&Alphanumeric)
        .filter(|c| !c.is_ascii_lowercase())
        .take(nchar)
        .collect()
}

fn random_string(nchar: usize) -> String {
    thread_rng()
        .sample_iter(&Alphanumeric)
        .take(nchar)
        .collect()
}

impl Verification {
    pub fn new(type_: Type) -> Verification {
        Verification {
            id: Uuid::new_v4(),
            created_time: Utc::now(),
            type_: type_,
            challenge: None,
            response: None,
            evidence: vec![],
            is_complete: false,
        }
    }

    pub fn type_(&self) -> &Type { &self.type_ }

    fn send_twilio_challenge(to_phone_number: &str,
                             challenge: &str) -> Result<String> {
        // curl -XPOST https://api.twilio.com/2010-04-01/Accounts/ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/Messages.json \
        // --data-urlencode "To=+13105555555" \
        // --data-urlencode "From=+12125551234" \
        // --data-urlencode "MediaUrl=https://demo.twilio.com/owl.png" \
        // --data-urlencode "Body=Hello from my Twilio line!" \
        // -u 'ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX:your_auth_token'
        let (account_sid, auth_token, from_phone_number) = (
            env::var("TWILIO_ACCOUNT_SID")?,
            env::var("TWILIO_AUTH_TOKEN")?,
            env::var("TWILIO_PHONE_NUMBER")?,
        );
        let msg = format!(
            "Your freeclaims.org verification code is: {}", challenge);
        let url = format!(
            "https://api.twilio.com/2010-04-01/Accounts/{}/Messages.json",
            account_sid);
        let params = [("From", from_phone_number),
                      ("To", to_phone_number.to_string()),
                      ("Body", msg)];
        let client = reqwest::Client::new();
        Ok(client.post(&url)
            .basic_auth(account_sid, Some(auth_token))
            .form(&params)
            .send()?
            .text()?)
    }

    fn send_mailgun_challenge(to_email: &str,
                              challenge: &str) -> Result<String> {
        /*
        curl -s --user 'api:YOUR_API_KEY' \
        https://api.mailgun.net/v3/YOUR_DOMAIN_NAME/messages \
        -F from='Excited User <mailgun@YOUR_DOMAIN_NAME>' \
        -F to=YOU@YOUR_DOMAIN_NAME \
        -F to=bar@example.com \
        -F subject='Hello' \
        -F text='Testing some Mailgun awesomeness!'
        */
        let (api_key, from_email) = (
            env::var("MAILGUN_API_KEY")?,
            env::var("MAILGUN_EMAIL")?,
        );
        let msg = format!(
            "Your freeclaims.org verification code is: {}", challenge);
        let url = "https://api.mailgun.net/v3/verify.freeclaims.org/messages";
        let params = [("from", from_email),
                      ("to", to_email.to_string()),
                      ("subject", "freeclaims.org verification".to_string()),
                      ("text", msg)];
        let client = reqwest::Client::new();
        Ok(client.post(url)
            .basic_auth("api", Some(api_key))
            .form(&params)
            .send()?
            .text()?)
    }

    fn send_dns_challenge(domain: &str, challenge: &str) -> Result<()> {
        Ok(())
    }

    pub fn check_dns(domain: &str, challenge: &str) -> Result<bool> {
        let dns_server_address = "8.8.8.8:53".parse()?;
        let conn = UdpClientConnection::new(dns_server_address)?;
        let client = DNSSyncClient::new(conn);
        let name = Name::from_str("www.example.com")?;
        let response: DnsResponse = client.query(
            &name, DNSClass::IN, RecordType::TXT)?;
        let answers: &[Record] = response.answers();
        if let &RData::TXT(ref txt) = answers[0].rdata() {
            for line in txt.iter() {
                if str::from_utf8(line)? == challenge {
                    return Ok(true)
                }
            }
        }
        Ok(false)
    }

    fn send_oauth2_challenge(issuer: &str, username: &str,
                             challenge: &str) -> Result<()> {
        Ok(())
    }

    pub fn issue_challenge(&mut self) -> Result<()> {
        if let Some(_) = self.challenge {
            // prevent some verification spam
            return Ok(());
        }

        let log_msg: String;
        let challenge: String;
        match &self.type_ {
            Type::Twilio { phone_number } => {
                challenge = random_6digit_code();
                log_msg = format!(
                    "Sending challenge code '{}' to phone number '{}'.",
                    challenge, phone_number);
                Self::send_twilio_challenge(&phone_number, &challenge)
                    .map(|resp| println!("Twilio response: \n{}", resp))?;
            },
            Type::Mailgun { email } => {
                challenge = random_string_upper(8);
                log_msg = format!(
                    "Sending challenge code '{}' to email '{}'.",
                    challenge, email);
                Self::send_mailgun_challenge(&email, &challenge)?;
            },
            Type::Dns { domain } => {
                // TXT record under domain
                challenge = format!(
                    "freeclaims.org-site-verification={}",
                    random_string(32));
                log_msg = format!(
                    "Sending challenge for domain '{}' to \
                     contain record '{}'.", domain, challenge);
                Self::send_dns_challenge(&domain, &challenge)?
            },
            Type::OAuth2 { issuer, username } => {
                challenge = String::new();
                log_msg = format!(
                    "Attempting to OAuth2 authenticate username \
                     '{}' at issuer '{}'.", issuer, username);
                Self::send_oauth2_challenge(&issuer, &username, &challenge)?
            },
        }
        self.evidence.push((Utc::now(), log_msg));
        self.challenge = Some(challenge);
        Ok(())
    }

    pub fn receive_response(&mut self, resp: &str) -> bool {
        match &self.challenge {
            None => return false,
            Some(challenge) if challenge != resp => return false,
            _ => (),
        }
        let log_msg = match &self.type_ {
            Type::Twilio { phone_number: _ } => {
                format!("User responded correctly: '{}'.", resp)
            },
            Type::Mailgun { email: _ } => {
                format!("User responded correctly: '{}'.", resp)
            },
            Type::Dns { domain } => {
                format!("'{}' DNS record is correct: '{}'.", domain, resp)
            },
            Type::OAuth2 { issuer, username } => {
                format!("Issuer '{}' successfully authenticated '{}'.",
                        issuer, username)
            },
        };
        self.evidence.push((Utc::now(), log_msg));
        self.is_complete = true;
        return true;
    }

    pub fn to_credential(&self, subject_id: &str) -> credential::Credential {
        let (ps, ct) = match &self.type_ {
            Type::Twilio { phone_number } => {
                (btreemap! {
                    "phoneNumber".to_string() => json!(phone_number),
                }, "CompletedSMSChallengeCredential")
            },
            Type::Mailgun { email } => {
                (btreemap! {
                    "email".to_string() => json!(email),
                }, "CompletedEmailChallengeCredential")
            },
            Type::Dns { domain } => {
                (btreemap! {
                    "domain".to_string() => json!(domain),
                }, "CompletedDnsChallengeCredential")
            },
            _ => unimplemented!(),
        };
        let cs = Subject {
            id: subject_id.to_string(),
            property_set: Some(ps),
        };
        /*
        let proof = Proof {
            type_: "".to_string(),
            created: Utc::now(),
            proof_purpose: "".to_string(),
            verification_method: "".to_string(),
            challenge: None,
            domain: None,
            jws: "".to_string(),
        };
        */
        let evidence_list = self.evidence.iter()
            .map(|e| {
                let mut ps = BTreeMap::new();
                ps.insert("time".to_string(), json!(e.0));
                ps.insert("message".to_string(), json!(e.1));
                credential::Evidence {
                    id: None,
                    type_: "EventLog".to_string(),
                    property_set: Some(ps),
                }
            }).collect();
        let cred = Credential {
            context: vec![
                "https://www.w3.org/2018/credentials/v1".to_string(),
            ],
            id: "freeclaims.org".to_string(),
            type_: vec![
                "VerifiableCredential".to_string(),
                ct.to_string(),
            ],
            issuer: "https://freeclaims.org/".to_string(),
            issuance_date: Utc::now(),
            expiration_date: None,
            credential_subject: cs,
            credential_status: None,
            proof: None,
            terms_of_use: None,
            evidence: Some(evidence_list),
        };
        return cred;
    }
}
