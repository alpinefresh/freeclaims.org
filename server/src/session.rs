use crate::verify::Verification;
use crate::did::Document;

use chrono::prelude::*;
use serde::{Serialize, Deserialize};
use uuid::Uuid;
use std::error::Error;

#[derive(Serialize, Deserialize, Debug)]
pub struct Session {
    id: Uuid,
    created_time: DateTime<Utc>,
    last_activity_time: DateTime<Utc>,
    verifications: Vec<Verification>,
    accounts: Vec<Account>,
}

impl Session {
    pub fn new() -> Session {
        Session {
            id: Uuid::new_v4(),
            created_time: Utc::now(),
            last_activity_time: Utc::now(),
            verifications: vec![],
            accounts: vec![],
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Account {
    created_time: DateTime<Utc>,
    refresh_token: String,
    access_token: String,
    username: String,
    issuer: String,
}

impl Account {
    pub fn to_did(&self) -> String {
        format!("did:oauth2:{}~{}", self.issuer, self.username)
    }

    pub fn to_did_document(&self) -> Document {
        Document::new(self.to_did())
    }
}
