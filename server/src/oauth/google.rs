use std::ops::Deref;
use super::profile;

use failure::{Error, err_msg};

use serde::{Serialize, Deserialize};

use actix_web::{web, HttpRequest, HttpResponse, Responder};
use actix_session::Session;

use url::Url;

use oauth2::prelude::*;
use oauth2::{
    AuthUrl,
    ClientId,
    ClientSecret,
    CsrfToken,
    RedirectUrl,
    Scope,
    TokenResponse,
    TokenUrl
};
use oauth2::basic::BasicClient;

use super::AuthCode;

#[derive(Deserialize, Clone)]
// TODO: could encrypt this and only decrypt upon access or something cool like that
pub struct GoogleClientData {
    pub id: String,
    pub secret: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GoogleUserProfile {
    pub name: String,
    pub email: String,
}

impl GoogleUserProfile {
    // TODO: change to use actual from_str impl or implement deserialize
    pub fn from_json(data: &str) -> Result<GoogleUserProfile, Error> {
        let data: GoogleJson = serde_json::from_str(data)?;
        if data.names.is_empty() {
            return Err(err_msg("Missing names in response from Google API"));
        }
        if data.emails.is_empty() {
            return Err(err_msg("Missing emails in response from Google API"));
        }

        // TODO: find primary name/email instead of first
        Ok(GoogleUserProfile {
            name: data.names[0].displayName.clone(),
            email: data.emails[0].email.clone(),
        })
    }
}


impl profile::ToDid for GoogleUserProfile {
    fn to_did(&self) -> String {
        format!("did:oauth2:{},{}", "accounts.google.com", self.email)
    }
}

// intermediate structs for deserializing user profile from json returned by google people api

fn make_false() -> bool { false }

/// Describes the type of Google account a piece of data is from.
#[derive(Debug, Clone, Deserialize)]
#[allow(non_camel_case_types)]
enum GoogleSourceType {
    SOURCE_TYPE_UNSPECIFIED,
    ACCOUNT,
    PROFILE,
    DOMAIN_PROFILE,
    CONTACT,
}

/// Describes what kind of Google account a piece of data is from.
#[derive(Debug, Clone, Deserialize)]
struct GoogleSourceMetadata {
    #[serde(rename = "type")]
    pub source_type: GoogleSourceType,
    pub id: String,
}

#[derive(Debug, Clone, Deserialize)]
struct GoogleFieldMetadata {
    #[serde(default = "make_false")]
    pub primary: bool,
    pub source: GoogleSourceMetadata,
}


#[derive(Debug, Clone, Deserialize)]
#[allow(non_snake_case)]
struct GoogleProfileName {
    pub metadata: GoogleFieldMetadata,
    pub displayName: String,
}

#[derive(Debug, Clone, Deserialize)]
struct GoogleProfileEmail {
    pub metadata: GoogleFieldMetadata,
    #[serde(rename = "value")]
    pub email: String,
}

#[derive(Debug, Clone, Deserialize)]
struct GoogleJson {
    names: Vec<GoogleProfileName>,
    #[serde(rename = "emailAddresses")]
    emails: Vec<GoogleProfileEmail>,
}

// Google Oauth2 authorization flow

#[derive(Debug, Clone)]
pub struct GoogleOauthClient(BasicClient);
impl Deref for GoogleOauthClient {
    type Target = BasicClient;
    fn deref(&self) -> &Self::Target { &self.0 }
}


/// Create an `oauth2` `BasicClient` configured to use Google's oauth endpoint. Currently hardcoded
/// to request the `profile` and `email` scopes.
pub fn make_google_client(google_client_data: GoogleClientData, domain: &str) -> GoogleOauthClient {
    let google_client_id = ClientId::new(google_client_data.id);
    let google_client_secret = ClientSecret::new(google_client_data.secret);
    let auth_url = AuthUrl::new(
        Url::parse("https://accounts.google.com/o/oauth2/v2/auth")
            .expect("Invalid authorization endpoint URL"),
    );
    let token_url = TokenUrl::new(
        Url::parse("https://www.googleapis.com/oauth2/v3/token")
            .expect("Invalid token endpoint URL"),
    );

    let client = BasicClient::new(
        google_client_id,
        Some(google_client_secret),
        auth_url,
        Some(token_url),
    )
    .set_redirect_url(RedirectUrl::new(
        Url::parse(&format!("http://{}/authorized/google", domain)).expect("Invalid redirect URL"),
    ))
    .add_scope(Scope::new("profile".to_string()))
    .add_scope(Scope::new("email".to_string()));

    GoogleOauthClient(client)
}

/// Redirect the user to a Google Oauth endpoint
pub fn redirect_google_auth_view(_req: HttpRequest, google_client: web::Data<GoogleOauthClient>) -> impl Responder {
    // Generate the authorization URL to which we'll redirect the user.
    let (authorize_url, _csrf_state) = google_client.authorize_url(CsrfToken::new_random);
    // TODO: store crsf state (in cookies? or in db?) and check it is the same in the authorization
    // code response at /authorized.

    HttpResponse::SeeOther()
        .header("Location", authorize_url.to_string())
        .finish()
        
}

/// The page the user is redirected to after the user authenticates with Google's Oauth endpoint
/// and authorizes our application.
pub fn user_authorized_google_view(_req: HttpRequest, session: Session, auth_code: web::Query<AuthCode>, google_client: web::Data<GoogleOauthClient>) -> impl Responder {
    let token_result =
        google_client.exchange_code(auth_code.clone().into_code())
        .expect("Failed to exchange code for oauth token");

    let access_token = token_result.access_token().secret();
    println!("Got token {}", access_token);

    if let Some(refresh_token) = token_result.refresh_token() {
        println!("Got refresh token {}", refresh_token.secret());
    }
    else {
        println!("no refresh token");
    }

    session.clear();
    session.set::<String>("google_oauth", access_token.into()).unwrap();

    HttpResponse::Found()
        .header("Location", "/google_credential")
        .finish()
}

#[cfg(test)]
mod test {
    use super::GoogleUserProfile;
    #[test]
    // just a basic sanity check with "anonymized" data
    fn test_google_profile_deserialize_1() {
        let input_json = r#"
    {
      "resourceName": "people/999999999",
      "etag": "%AAAAAAAAAAAAA",
      "names": [
        {
          "metadata": {
            "primary": true,
            "source": {
              "type": "PROFILE",
              "id": "1111"
            }
          },
          "displayName": "Harry Stern",
          "familyName": "Stern",
          "givenName": "Harry",
          "displayNameLastFirst": "Stern, Harry"
        },
        {
          "metadata": {
            "source": {
              "type": "DOMAIN_PROFILE",
              "id": "1111"
            }
          },
          "displayName": "Harry Stern",
          "familyName": "Stern",
          "givenName": "Harry",
          "displayNameLastFirst": "Harry Stern"
        }
      ],
      "emailAddresses": [
        {
          "metadata": {
            "primary": true,
            "verified": true,
            "source": {
              "type": "DOMAIN_PROFILE",
              "id": "1111"
            }
          },
          "value": "harry.stern@example.com"
        }
      ]
    }"#;

    let profile_opt = GoogleUserProfile::from_json(&input_json);
    assert!(profile_opt.is_ok());

    let profile = profile_opt.unwrap();
    assert_eq!(profile.name, "Harry Stern");
    assert_eq!(profile.email, "harry.stern@example.com");

    }

    #[test]
    // second basic sanity check with "anonymized" data
    fn test_google_profile_deserialize_2() {
        let input_json = r#"
    {
  "resourceName": "people/77777777",
  "etag": "%AAAAAAAAAAAAAAA",
  "names": [
    {
      "metadata": {
        "primary": true,
        "source": {
          "type": "PROFILE",
          "id": "2222"
        }
      },
      "displayName": "Someone Else",
      "familyName": "Someone",
      "givenName": "Else",
      "displayNameLastFirst": "Else, Someone"
    }
  ],
  "emailAddresses": [
    {
      "metadata": {
        "primary": true,
        "verified": true,
        "source": {
          "type": "ACCOUNT",
          "id": "2222"
        }
      },
      "value": "different.harry.stern@example.com"
    }
  ]
}"#;

    let profile_opt = GoogleUserProfile::from_json(&input_json);
    assert!(profile_opt.is_ok());

    let profile = profile_opt.unwrap();
    assert_eq!(profile.name, "Someone Else");
    assert_eq!(profile.email, "different.harry.stern@example.com");

    }
}
