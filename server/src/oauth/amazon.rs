use std::ops::Deref;
use super::profile;
use serde::{Serialize, Deserialize};

use actix_web::{web, HttpRequest, HttpResponse, Responder};
use actix_session::Session;

use url::Url;

use oauth2::prelude::*;
use oauth2::{
    AuthUrl,
    ClientId,
    ClientSecret,
    CsrfToken,
    RedirectUrl,
    Scope,
    TokenResponse,
    TokenUrl
};
use oauth2::basic::BasicClient;

use super::AuthCode;

#[derive(Deserialize, Clone)]
// TODO: could encrypt this and only decrypt upon access or something cool like that
/// Amazon Oauth client configuration
pub struct AmazonClientData {
    pub id: String,
    pub secret: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct AmazonUserProfile {
    pub email: String,
    pub name: String,
    user_id: String,
}

impl profile::ToDid for AmazonUserProfile {
    fn to_did(&self) -> String {
        format!("did:oauth2:{},{}", "www.amazon.com", self.email)
    }
}

#[derive(Debug, Clone)]
pub struct AmazonOauthClient(BasicClient);
impl Deref for AmazonOauthClient {
    type Target = BasicClient;
    fn deref(&self) -> &Self::Target { &self.0 }
}

/// Create an `oauth2` `BasicClient` configured to use Amazon's oauth endpoint. Currently hardcoded
/// to request the `profile` and `email` scopes.
pub fn make_amazon_client(amazon_client_data: AmazonClientData, domain: &str) -> AmazonOauthClient {
    let amazon_client_id = ClientId::new(amazon_client_data.id);
    let amazon_client_secret = ClientSecret::new(amazon_client_data.secret);
    let auth_url = AuthUrl::new(
        Url::parse("https://www.amazon.com/ap/oa")
            .expect("Invalid authorization endpoint URL"),
    );
    let token_url = TokenUrl::new(
        Url::parse("https://api.amazon.com/auth/o2/token")
            .expect("Invalid token endpoint URL"),
    );

    let client = BasicClient::new(
        amazon_client_id,
        Some(amazon_client_secret),
        auth_url,
        Some(token_url),
    )
    .set_redirect_url(RedirectUrl::new(
        Url::parse(&format!("http://{}/authorized/amazon", domain)).expect("Invalid redirect URL"),
    ))
    .add_scope(Scope::new("profile".to_string()));

    AmazonOauthClient(client)
}

/// Redirect the user to a Amazon Oauth endpoint
pub fn redirect_amazon_auth_view(_req: HttpRequest, amazon_client: web::Data<AmazonOauthClient>) -> impl Responder {
    // Generate the authorization URL to which we'll redirect the user.
    let (authorize_url, _csrf_state) = amazon_client.authorize_url(CsrfToken::new_random);
    // TODO: store crsf state (in cookies? or in db?) and check it is the same in the authorization
    // code response at /authorized.

    HttpResponse::SeeOther()
        .header("Location", authorize_url.to_string())
        .finish()

}

/// The page the user is redirected to after the user authenticates with Amazon's Oauth endpoint
/// and authorizes our application.
pub fn user_authorized_amazon_view(_req: HttpRequest, session: Session, auth_code: web::Query<AuthCode>, amazon_client: web::Data<AmazonOauthClient>) -> impl Responder {
    let token_result =
        amazon_client.exchange_code(auth_code.clone().into_code())
        .expect("Failed to exchange code for oauth token");

    let access_token = token_result.access_token().secret();
    println!("Got token {}", access_token);

    if let Some(refresh_token) = token_result.refresh_token() {
        println!("Got refresh token {}", refresh_token.secret());
    }
    else {
        println!("no refresh token");
    }

    session.clear();
    session.set::<String>("amazon_oauth", access_token.into()).unwrap();

    HttpResponse::Found()
        .header("Location", "/amazon_credential")
        .finish()
}
