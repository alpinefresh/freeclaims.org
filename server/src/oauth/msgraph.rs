use std::ops::Deref;
use super::profile;

use serde::{Serialize, Deserialize};

use actix_web::{web, HttpRequest, HttpResponse, Responder};
use actix_session::Session;

use url::Url;

use oauth2::prelude::*;
use oauth2::{
    AuthUrl,
    ClientId,
    ClientSecret,
    CsrfToken,
    RedirectUrl,
    Scope,
    TokenResponse,
    TokenUrl
};
use oauth2::basic::BasicClient;

use super::AuthCode;

#[derive(Deserialize, Clone)]
// TODO: could encrypt this and only decrypt upon access or something cool like that
/// MSGraph Oauth client configuration
pub struct MSGraphClientData {
    pub id: String,
    pub secret: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct MSGraphUserProfile {
    // NB: The "mail" field is an entry set in AD and may be anything.
    // "UserPrincipalName" is what users use to sign in and I'm pretty sure should always be in the
    // form of an email address.
    // See <https://docs.microsoft.com/en-us/azure/active-directory/hybrid/plan-connect-userprincipalname>
    //
    // #[serde(rename = "mail")]
    // pub email: Option<String>,
    #[serde(rename = "userPrincipalName")]
    pub email: String,
    #[serde(rename = "displayName")]
    pub name: String,
}


impl profile::ToDid for MSGraphUserProfile {
    fn to_did(&self) -> String {
        format!("did:oauth2:{},{}", "login.microsoftonline.com", self.email)
    }
}

#[derive(Debug, Clone)]
pub struct MSGraphOauthClient(BasicClient);
impl Deref for MSGraphOauthClient {
    type Target = BasicClient;
    fn deref(&self) -> &Self::Target { &self.0 }
}

/// Create an `oauth2` `BasicClient` configured to use MSGraph's oauth endpoint. Currently hardcoded
/// to request the `profile` and `email` scopes.
pub fn make_msgraph_client(msgraph_client_data: MSGraphClientData, domain: &str) -> MSGraphOauthClient {
    let msgraph_client_id = ClientId::new(msgraph_client_data.id);
    let msgraph_client_secret = ClientSecret::new(msgraph_client_data.secret);
    let auth_url = AuthUrl::new(
        Url::parse("https://login.microsoftonline.com/common/oauth2/v2.0/authorize")
            .expect("Invalid authorization endpoint URL"),
    );
    let token_url = TokenUrl::new(
        Url::parse("https://login.microsoftonline.com/common/oauth2/v2.0/token")
            .expect("Invalid token endpoint URL"),
    );

    let client = BasicClient::new(
        msgraph_client_id,
        Some(msgraph_client_secret),
        auth_url,
        Some(token_url),
    )
    .set_redirect_url(RedirectUrl::new(
        Url::parse(&format!("http://{}/authorized/msgraph", domain)).expect("Invalid redirect URL"),
    ))
    .add_scope(Scope::new("User.Read".to_string()));

    MSGraphOauthClient(client)
}

/// Redirect the user to an MSGraph Oauth endpoint
pub fn redirect_msgraph_auth_view(_req: HttpRequest, msgraph_client: web::Data<MSGraphOauthClient>) -> impl Responder {
    // Generate the authorization URL to which we'll redirect the user.
    let (authorize_url, _csrf_state) = msgraph_client.authorize_url(CsrfToken::new_random);
    // TODO: store crsf state (in cookies? or in db?) and check it is the same in the authorization
    // code response at /authorized.

    HttpResponse::SeeOther()
        .header("Location", authorize_url.to_string())
        .finish()

}

/// The page the user is redirected to after the user authenticates with MSGraph's Oauth endpoint
/// and authorizes our application.
pub fn user_authorized_msgraph_view(_req: HttpRequest, session: Session, auth_code: web::Query<AuthCode>, msgraph_client: web::Data<MSGraphOauthClient>) -> impl Responder {
    let token_result =
        msgraph_client.exchange_code(auth_code.clone().into_code())
        .expect("Failed to exchange code for oauth token");

    let access_token = token_result.access_token().secret();
    println!("Got token {}", access_token);

    if let Some(refresh_token) = token_result.refresh_token() {
        println!("Got refresh token {}", refresh_token.secret());
    }
    else {
        println!("no refresh token");
    }

    session.clear();
    session.set::<String>("msgraph_oauth", access_token.into()).unwrap();

    HttpResponse::Found()
        .header("Location", "/msgraph_credential")
        .finish()
}


#[cfg(test)]
mod tests {
    use super::MSGraphUserProfile;
    #[test]
    fn test_msgraph_deserialize_1() {
        let input_json = r#"
{
    "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#users/$entity",
    "businessPhones": [
        "+1 412 555 0109"
    ],
    "displayName": "Megan Bowen",
    "givenName": "Megan",
    "jobTitle": "Auditor",
    "mail": "MeganB@M365x214355.onmicrosoft.com",
    "mobilePhone": null,
    "officeLocation": "12/1110",
    "preferredLanguage": "en-US",
    "surname": "Bowen",
    "userPrincipalName": "MeganB@M365x214355.onmicrosoft.com",
    "id": "48d31887-5fad-4d73-a9f5-3c356e68a038"
}"#;

        let profile_res: Result<MSGraphUserProfile,_> = serde_json::from_str(&input_json);
        assert!(profile_res.is_ok());

        let profile = profile_res.unwrap();
        assert_eq!(profile.name, "Megan Bowen");
        assert_eq!(profile.email, "MeganB@M365x214355.onmicrosoft.com");
    }
}
