use crate::did;

pub trait ToDid {
    fn to_did(&self) -> String;
    fn to_did_document(&self) -> did::Document {
        did::Document::new(self.to_did())
    }
}
