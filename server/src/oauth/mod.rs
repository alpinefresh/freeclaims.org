pub mod google;
pub mod msgraph;
pub mod amazon;
pub mod facebook;
pub mod profile;

use serde::Deserialize;
use oauth2::AuthorizationCode;

#[derive(Clone, Deserialize)]
/// This struct is a wrapper for use in `actix_web::web::Query<T>` parameters. It is necessary due
/// to the fact that tuple structs cannot be deserialized from query strings (there's no name to
/// read). See <https://docs.rs/actix-web/1.0.8/actix_web/web/struct.Query.html>
pub struct AuthCode {
    code: AuthorizationCode,
}

impl AuthCode {
    pub fn into_code(self) -> AuthorizationCode {
        self.code
    }
}
