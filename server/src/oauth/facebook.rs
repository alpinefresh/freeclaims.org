use std::ops::Deref;
use super::profile;

use serde::{Serialize, Deserialize};

use actix_web::{web, HttpRequest, HttpResponse, Responder};
use actix_session::Session;

use url::Url;

use oauth2::prelude::*;
use oauth2::{
    AuthUrl,
    ClientId,
    ClientSecret,
    CsrfToken,
    RedirectUrl,
    Scope,
    TokenResponse,
    TokenUrl
};
use oauth2::basic::BasicClient;

use super::AuthCode;

#[derive(Deserialize, Clone)]
// TODO: could encrypt this and only decrypt upon access or something cool like that
/// Facebook Oauth client configuration
pub struct FacebookClientData {
    pub id: String,
    pub secret: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct FacebookUserProfile {
    pub email: String,
    pub name: String,
    user_id: String,
}


impl profile::ToDid for FacebookUserProfile {
    fn to_did(&self) -> String {
        format!("did:oauth2:{},{}", "www.facebook.com", self.email)
    }
}

#[derive(Debug, Clone)]
pub struct FacebookOauthClient(BasicClient);
impl Deref for FacebookOauthClient {
    type Target = BasicClient;
    fn deref(&self) -> &Self::Target { &self.0 }
}
/// Create an `oauth2` `BasicClient` configured to use Facebook's oauth endpoint. Currently hardcoded
/// to request the `profile` and `email` scopes.
pub fn make_facebook_client(facebook_client_data: FacebookClientData, domain: &str) -> FacebookOauthClient {
    let facebook_client_id = ClientId::new(facebook_client_data.id);
    let facebook_client_secret = ClientSecret::new(facebook_client_data.secret);
    let auth_url = AuthUrl::new(
        Url::parse("https://www.facebook.com/v4.0/dialog/oauth")
            .expect("Invalid authorization endpoint URL"),
    );
    let token_url = TokenUrl::new(
        Url::parse("https://graph.facebook.com/oauth/access_token")
            .expect("Invalid token endpoint URL"),
    );

    let client = BasicClient::new(
        facebook_client_id,
        Some(facebook_client_secret),
        auth_url,
        Some(token_url),
    )
    .set_redirect_url(RedirectUrl::new(
        Url::parse(&format!("{}/authorized/facebook", domain)).expect("Invalid redirect URL"),
    ))
    .add_scope(Scope::new("public_profile".to_string()))
    .add_scope(Scope::new("email".to_string()));

    FacebookOauthClient(client)
}

/// Redirect the user to a Facebook Oauth endpoint
pub fn redirect_facebook_auth_view(_req: HttpRequest, facebook_client: web::Data<FacebookOauthClient>) -> impl Responder {
    // Generate the authorization URL to which we'll redirect the user.
    let (authorize_url, _csrf_state) = facebook_client.authorize_url(CsrfToken::new_random);
    // TODO: store crsf state (in cookies? or in db?) and check it is the same in the authorization
    // code response at /authorized.

    HttpResponse::SeeOther()
        .header("Location", authorize_url.to_string())
        .finish()
        
}

/// The page the user is redirected to after the user authenticates with Facebook's Oauth endpoint
/// and authorizes our application.
pub fn user_authorized_facebook_view(_req: HttpRequest, session: Session, auth_code: web::Query<AuthCode>, facebook_client: web::Data<FacebookOauthClient>) -> impl Responder {
    let token_result =
        facebook_client.exchange_code(auth_code.clone().into_code())
        .expect("Failed to exchange code for oauth token");

    let access_token = token_result.access_token().secret();
    println!("Got token {}", access_token);

    if let Some(refresh_token) = token_result.refresh_token() {
        println!("Got refresh token {}", refresh_token.secret());
    }
    else {
        println!("no refresh token");
    }

    session.clear();
    session.set::<String>("facebook_oauth", access_token.into()).unwrap();

    HttpResponse::Found()
        .header("Location", "/facebook_credential")
        .finish()
}
