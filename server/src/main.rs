extern crate jsonwebtoken as jwt;
use jwt::{encode, decode, Header, Algorithm, Validation};

use oauth::google::{GoogleClientData, GoogleUserProfile, make_google_client, user_authorized_google_view, redirect_google_auth_view};
use oauth::amazon::{AmazonClientData, AmazonUserProfile, make_amazon_client, user_authorized_amazon_view, redirect_amazon_auth_view};
use oauth::facebook::{FacebookClientData, FacebookUserProfile, make_facebook_client, user_authorized_facebook_view, redirect_facebook_auth_view};
use oauth::msgraph::{MSGraphClientData, MSGraphUserProfile, make_msgraph_client, user_authorized_msgraph_view, redirect_msgraph_auth_view};

use actix_web::{web, App, HttpRequest, HttpResponse, HttpServer, Responder, guard};
use actix_web::error::Result;
use actix_session::{Session, CookieSession};


use failure::err_msg;

use serde::{Serialize, Deserialize};
use dotenv;
use std::env;

use verify::Verification;
use askama::Template;

mod did;
mod credential;
mod verify;
mod session;
mod oauth;

use oauth::profile;
use oauth::profile::ToDid;
use hex;

#[derive(Deserialize, Clone)]
struct ServerConfig {
    google_client: GoogleClientData,
    amazon_client: AmazonClientData,
    facebook_client: FacebookClientData,
    msgraph_client: MSGraphClientData,

    server_session_key: String,
    bind_address: String,
    bind_port: u16,
    domain: String,
}

#[derive(Template)]
#[template(path = "index.html")]
struct Index;

#[derive(Template)]
#[template(path = "about.html")]
struct About;

// technically these two don't need to be a template but having it as a template means the
// existance of the file is checked at compile time
#[derive(Template)]
#[template(path = "login_providers.html")]
struct LoginProviders;

#[derive(Template)]
#[template(path = "verification_providers.html")]
struct VerificationProviders;

#[derive(Template)]
#[template(path = "verify_sms_start.html")]
struct VerifySmsStart {}

#[derive(Template)]
#[template(path = "verify_sms_input.html")]
struct VerifySmsInput {
    phone_number: String,
}


#[derive(Deserialize)]
struct VerifySmsInputFormData {
    pub phone_number: String,
    //response: String,
}

#[derive(Deserialize)]
struct VerifySmsVerifyFormData {
    pub verification_code: String,
}

fn index() -> impl Responder {
    // TODO: use HttpRequest and generate full url
    // maybe there are actix helpers to generate urls
    let body = Index.render().unwrap();
    HttpResponse::Ok().content_type("text/html").body(body)
}

fn about() -> impl Responder {
    // TODO: use HttpRequest and generate full url
    // maybe there are actix helpers to generate urls
    let body = About.render().unwrap();
    HttpResponse::Ok().content_type("text/html").body(body)
}


fn logout(session: Session) -> impl Responder {
    session.clear();
    "Session cleared"
}

fn login_start_view() -> impl Responder {
    HttpResponse::Ok().content_type("text/html").body(LoginProviders.render().unwrap())
}

fn verify_start_view() -> impl Responder {
    HttpResponse::Ok().content_type("text/html").body(VerificationProviders.render().unwrap())
}

/// Combine the identity data and verification data to issue the credential
fn issue_credential(_req: HttpRequest, session: Session) -> impl Responder {
    let user_profile = match session.get::<UserProfile>("user_profile").unwrap() {
        Some(user_profile) => user_profile,
        None => return HttpResponse::BadRequest().body("Missing user profile in session"),
    };

    let verification = match session.get::<Verification>("verifier").unwrap() {
        Some(verification) => verification,
        None => return HttpResponse::BadRequest().body("Missing verifier in session"),
    };

    // TODO: remove this garbage hack and replace with real signing
    let vc = verification.to_credential(&user_profile_to_did(&user_profile));
    let secret = hex::decode(env::var("SIGNING_SECRET").unwrap()).unwrap();
    let token = encode(&Header::default(), &vc, (&secret).as_ref()).unwrap();
    // do something with user_profile and verification here

    return HttpResponse::Ok()
        .body(format!("DID:\n{}\n\nVC:\n{}\n\nJWT:\n{}",
                serde_json::to_string(&user_profile_to_did_document(&user_profile)).expect("failed to serialize did doc"),
                serde_json::to_string_pretty(&vc).expect("failed to serialize vc"),
                token,
              )
        );
    /*
        .body(format!("Issue a credential with identity:\n {}\n and verification data:\n {}\n\nDID:\n{}\n\nVC:\n{}\n\nJWT:\n{}",
                serde_json::to_string(&user_profile).expect("failed to serialize user profile"),
                serde_json::to_string(&verification).expect("failed to serialize verification"),
                serde_json::to_string(&user_profile_to_did_document(&user_profile)).expect("failed to serialize did doc"),
                serde_json::to_string_pretty(&vc).expect("failed to serialize vc"),
                token,
              )
        );
    */
}

fn verify_sms_start_view(_req: HttpRequest, _session: Session) -> impl Responder {
    let tmpl = VerifySmsStart {};
    tmpl.render()
        .map(|body| HttpResponse::Ok().content_type("text/html").body(body))
        .map_err(|e| err_msg(e.to_string()))
}

fn verify_sms_input_code_view(_req: HttpRequest, session: Session,
              form: web::Form<VerifySmsInputFormData>
              ) -> Result<HttpResponse>  {
   
    let phone_number = form.phone_number.clone();
    let mut verifier = Verification::new(verify::Type::Twilio { phone_number });

    let challenge_res = verifier.issue_challenge();
    println!("{:?}", challenge_res);

    session.remove("verifier");
    session.set("verifier", verifier).unwrap();

    let phone_number = form.phone_number.clone();
    let tmpl = VerifySmsInput { phone_number };

    let body = tmpl.render().unwrap();
    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}

fn verify_sms_verify_code_view(_req: HttpRequest, session: Session, form: web::Form<VerifySmsVerifyFormData>) -> impl Responder {
    let code = &form.verification_code;

    let mut verifier = match session.get::<Verification>("verifier").unwrap() {
        Some(verifier) => verifier,
        None => return HttpResponse::BadRequest().body("No verifier session data")
    };

    let code_verified = verifier.receive_response(code);

    // success
    if code_verified {
        session.remove("verifier");
        session.set("verifier", verifier).unwrap();
        HttpResponse::SeeOther()
            .header("Location", "/issue_credential")
            .finish()
    } else {
        // show "verification failed" message, show input box and link back to new code page
        HttpResponse::Ok()
            .body("Verification failed")
    }
}

#[derive(Template)]
#[template(path = "verify_email_start.html")]
struct VerifyEmailStart {}

#[derive(Template)]
#[template(path = "verify_email_input.html")]
struct VerifyEmailInput {
    email: String,
}

#[derive(Deserialize)]
struct VerifyEmailInputFormData {
    pub email: String,
    //response: String,
}

#[derive(Deserialize)]
struct VerifyEmailVerifyFormData {
    pub verification_code: String,
}

fn verify_email_start_view(_req: HttpRequest, _session: Session) -> impl Responder {
    let tmpl = VerifyEmailStart {};
    tmpl.render()
        .map(|body| HttpResponse::Ok().content_type("text/html").body(body))
        .map_err(|e| err_msg(e.to_string()))
}

fn verify_email_input_code_view(_req: HttpRequest, session: Session,
              form: web::Form<VerifyEmailInputFormData>
              ) -> Result<HttpResponse>  {
   
    let email = form.email.clone();
    let mut verifier = Verification::new(verify::Type::Mailgun { email });

    let challenge_res = verifier.issue_challenge();
    println!("{:?}", challenge_res);

    session.remove("verifier");
    session.set("verifier", verifier).unwrap();

    let email = form.email.clone();
    let tmpl = VerifyEmailInput { email };

    let body = tmpl.render().unwrap();
    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}

fn verify_email_verify_code_view(_req: HttpRequest, session: Session, form: web::Form<VerifyEmailVerifyFormData>) -> impl Responder {
    let code = &form.verification_code;

    let mut verifier = match session.get::<Verification>("verifier").unwrap() {
        Some(verifier) => verifier,
        None => return HttpResponse::BadRequest().body("No verifier session data")
    };

    let code_verified = verifier.receive_response(code);

    // success
    if code_verified {
        session.remove("verifier");
        session.set("verifier", verifier).unwrap();
        HttpResponse::SeeOther()
            .header("Location", "/issue_credential")
            .finish()
    } else {
        // show "verification failed" message, show input box and link back to new code page
        HttpResponse::Ok()
            .body("Verification failed")
    }
}

fn verify_domain() -> impl Responder {
    HttpResponse::Ok().content_type("text/html").body(
        "Verify your phone number! <input type=\"text\"/>"
    )
}

#[derive(Serialize, Deserialize, Clone, Debug)]
enum UserProfile {
    Amazon(AmazonUserProfile),
    Google(GoogleUserProfile),
    MSGraph(MSGraphUserProfile),
    Facebook(FacebookUserProfile),
}

// TODO: fix this mess w/type system
fn user_profile_to_did(u: &UserProfile) -> String {
    match u {
        UserProfile::Amazon(p) => p.to_did(),
        UserProfile::Google(p) => p.to_did(),
        UserProfile::MSGraph(p) => p.to_did(),
        UserProfile::Facebook(p) => p.to_did(),
    }
}

fn user_profile_to_did_document(u: &UserProfile) -> did::Document {
    match u {
        UserProfile::Amazon(p) => p.to_did_document(),
        UserProfile::Google(p) => p.to_did_document(),
        UserProfile::MSGraph(p) => p.to_did_document(),
        UserProfile::Facebook(p) => p.to_did_document(),
    }
}

fn google_credential(_req: HttpRequest, session: Session) -> impl Responder {
    let api_url = "https://people.googleapis.com/v1/people/me";

    let access_token = match session.get::<String>("google_oauth").unwrap() {
        Some(access_token) => access_token,
        None => return "User not authenticated with Google".into(),
    };

    let client = reqwest::Client::new();
    let res = client.get(api_url)
        .bearer_auth(access_token)
        .query(&[("personFields", "names,emailAddresses")])
        .send();
    
    // TODO: use .json() with struct that derives Deserialize based on schema at
    // https://developers.google.com/people/api/rest/v1/people#Person
    let json_str = res.expect("hitting people api failed")
        .text().unwrap();
    let profile = GoogleUserProfile::from_json(&json_str).unwrap();

    session.set("user_profile", UserProfile::Google(profile)).unwrap();

    HttpResponse::SeeOther()
        .header("Location", "/verify")
        .finish()
}

fn amazon_credential(_req: HttpRequest, session: Session) -> impl Responder {
    let api_url = "https://api.amazon.com/user/profile";

    let access_token = match session.get::<String>("amazon_oauth").unwrap() {
        Some(access_token) => access_token,
        None => return "User not authenticated with Amazon".into(),
    };

    let client = reqwest::Client::new();
    let res = client.get(api_url)
        .bearer_auth(access_token)
        //.query() // TODO
        .send();

    // TODO: use .json() with struct that derives Deserialize based on
    // https://developer.amazon.com/docs/login-with-amazon/customer-profile.html
    let profile: AmazonUserProfile = res.expect("hitting amazon profile api failed")
        .json().unwrap();

    session.set("user_profile", UserProfile::Amazon(profile)).unwrap();

    HttpResponse::SeeOther()
        .header("Location", "/verify")
        .finish()
}

fn msgraph_credential(_req: HttpRequest, session: Session) -> impl Responder {
    let api_url = "https://graph.microsoft.com/v1.0/me";

    let access_token = match session.get::<String>("msgraph_oauth").unwrap() {
        Some(access_token) => access_token,
        None => return "User not authenticated with MS Graph".into(),
    };

    let client = reqwest::Client::new();
    let res = client.get(api_url)
        .bearer_auth(access_token)
        .send();

    let profile: MSGraphUserProfile = res.expect("hitting msgraph profile api failed")
        .json().unwrap();

    session.set("user_profile", UserProfile::MSGraph(profile)).unwrap();

    HttpResponse::SeeOther()
        .header("Location", "/verify")
        .finish()

}

fn facebook_credential(_req: HttpRequest, session: Session) -> impl Responder {
    let api_url = "https://graph.facebook.com/me";

    let access_token = match session.get::<String>("facebook_oauth").unwrap() {
        Some(access_token) => access_token,
        None => return "User not authenticated with Facebook".into(),
    };

    let client = reqwest::Client::new();
    let res = client.get(api_url)
        .bearer_auth(access_token)
        .query(&[("personFields", "names,emailAddresses")])
        .send();
    
    let profile: FacebookUserProfile = res.expect("hitting fb api failed")
        .json().unwrap();

    session.set("user_profile", UserProfile::Facebook(profile)).unwrap();

    HttpResponse::SeeOther()
        .header("Location", "/verify")
        .finish()
}

fn main() {
    let server_config: ServerConfig = toml::from_str(include_str!("../server_config.toml"))
        .expect("unable to parse server config");
    dotenv::from_filename("freeclaims.env").ok();
    let google_client = make_google_client(server_config.google_client, &server_config.domain);
    let amazon_client = make_amazon_client(server_config.amazon_client, &server_config.domain);
    let facebook_client = make_facebook_client(server_config.facebook_client, &server_config.domain);
    let msgraph_client = make_msgraph_client(server_config.msgraph_client, &server_config.domain);

    let session_key = server_config.server_session_key;

    HttpServer::new(move || {
        let google_client = google_client.clone();
        let amazon_client = amazon_client.clone();
        let facebook_client = facebook_client.clone();
        let msgraph_client = msgraph_client.clone();
        let session_key = session_key.as_bytes();
        App::new()
            .wrap(
                CookieSession::private(session_key)
                    .name("freeclaims-actix")
                    .path("/")
                    // set to true in production - only transmits cookies over https
                    .secure(false),
            )
            .route("/", web::get().to(index))

            .route("/about", web::get().to(about))

            .route("/login", web::get().to(login_start_view))
            .route("/verify", web::get().to(verify_start_view))
            .route("/issue_credential", web::get().to(issue_credential))

            .route("/logout", web::get().to(logout))

            .route("/verify/sms", web::get().to(verify_sms_start_view))
            .route("/verify/sms/input_code", web::post().to(verify_sms_input_code_view))
            .route("/verify/sms/verify_code", web::post().to(verify_sms_verify_code_view))

            .route("/verify/email", web::get().to(verify_email_start_view))
            .route("/verify/email/input_code", web::post().to(verify_email_input_code_view))
            .route("/verify/email/verify_code", web::post().to(verify_email_verify_code_view))

            .route("/verify/domain", web::to(verify_domain))

            // TODO put these routes into actix web Config structs and expose them in each
            // provider's mod
            //
            // Google login routes
            .route("/login/google", web::get().to(redirect_google_auth_view))
            .route("/authorized/google", web::get().to(user_authorized_google_view))
            .route("/google_credential", web::get().to(google_credential))
			
            // Amazon login routes
            .route("/login/amazon", web::get().to(redirect_amazon_auth_view))
            .route("/authorized/amazon", web::get().to(user_authorized_amazon_view))
            .route("/amazon_credential", web::get().to(amazon_credential))

            // Facebook login routes
            .route("/login/facebook", web::get().to(redirect_facebook_auth_view))
            .route("/authorized/facebook", web::get().to(user_authorized_facebook_view))
            .route("/facebook_credential", web::get().to(facebook_credential))

            // Amazon login routes
            .route("/login/msgraph", web::get().to(redirect_msgraph_auth_view))
            .route("/authorized/msgraph", web::get().to(user_authorized_msgraph_view))
            .route("/msgraph_credential", web::get().to(msgraph_credential))

            // just 404 by default
            .default_service(
                web::resource("")
                    // route gets to 404
                    .route(web::get().to(|| {
                        HttpResponse::NotFound().body("404 not found")
                    }))
                    // all requests that are not get to method not allowed
                    .route(
                        web::route()
                            .guard(guard::Not(guard::Get()))
                            .to(HttpResponse::MethodNotAllowed),
                    ),
            )

            .data(google_client)
            .data(amazon_client)
            .data(facebook_client)
            .data(msgraph_client)
    })
    .bind((server_config.bind_address.as_str(), server_config.bind_port))
    .expect("Can not bind to address or port")
    .run()
    .unwrap();
}

#[allow(dead_code)]
fn print_examples() {
    println!("--- vc ---");
    println!("{}", credential::create_example_credential());
    println!("--- simple document ---");
    println!("{}", did::create_simple_example_document());
    println!("--- document ---");
    println!("{}", did::create_example_document());
}
