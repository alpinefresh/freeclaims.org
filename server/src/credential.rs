use std::collections::BTreeMap as Map;

use serde::{Serialize, Deserialize};
use serde_json;
use serde_json::{Value, json};
use chrono::prelude::*;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Credential {
    #[serde(rename = "@context")]
    pub context: Vec<String>,
    pub id: String,

    #[serde(rename = "type")]
    pub type_: Vec<String>,

    pub issuer: String,

    pub issuance_date: DateTime<Utc>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub expiration_date: Option<DateTime<Utc>>,

    pub credential_subject: Subject,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub credential_status: Option<Status>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub proof: Option<Proof>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub terms_of_use: Option<Vec<TermsOfUse>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub evidence: Option<Vec<Evidence>>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TermsOfUse {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    #[serde(rename = "type")]
    pub type_: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Evidence {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    #[serde(rename = "type")]
    pub type_: String,

    #[serde(flatten)]
    pub property_set: Option<Map<String, Value>>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Subject {
    pub id: String,

    #[serde(flatten)]
    pub property_set: Option<Map<String, Value>>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Status {
    pub id: String,

    #[serde(rename = "type")]
    pub type_: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Proof {
    #[serde(rename = "type")]
    pub type_: String,
    pub created: DateTime<Utc>,
    pub proof_purpose: String,
    pub verification_method: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub challenge: Option<String>,
    
    #[serde(skip_serializing_if = "Option::is_none")]
    pub domain: Option<String>,

    pub jws: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Presentation {
    #[serde(rename = "@context")]
    pub context: Vec<String>,

    #[serde(rename = "type")]
    pub type_: String,

    pub verifiable_credential: Vec<Credential>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub proof: Option<Proof>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub holder: Option<String>,
}

pub fn create_example_credential() -> String {
    let mut ps = Map::new();
    ps.insert("alumniOf".to_string(), json!({
        "id": "did:example:c276e12ec21ebfeb1f712ebc6f1",
        "name": [
            {
                "value": "Example University",
                "lang": "en",
            },
            {
                "value": "Exemple d'Université",
                "lang": "fr",
            },
        ],
    }));
    let cred_subject = Subject {
        id: "did:example:ebfeb1f712ebc6f1c276e12ec21".to_string(),
        property_set: Some(ps),
    };
    let proof = Proof {
        type_: "RsaSignature2018".to_string(),
        created: "2017-06-18T21:19:10Z".parse::<DateTime<Utc>>().unwrap(),
        proof_purpose: "assertionMethod".to_string(),
        verification_method: "https://example.edu/issuers/keys/1".to_string(),
        challenge: None,
        domain: None,
        jws: "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..TCYt5XsITJX1CxPCT8yAV-TVkIEq_PbChOMqsLfRoPsnsgw5WEuts01mq-pQy7UJiN5mgRxD-WUcX16dUEMGlv50aqzpqh4Qktb3rk-BuQy72IFLOqV0G_zS245-kronKb78cPN25DGlcTwLtjPAYuNzVBAh4vGHSrQyHUdBBPM".to_string(),
    };
    let cred = Credential {
        context: vec![
            "https://www.w3.org/2018/credentials/v1".to_string(),
            "https://www.w3.org/2018/credentials/examples/v1".to_string(),
        ],
        id: "http://example.edu/credentials/1872".to_string(),
        type_: vec![
            "VerifiableCredential".to_string(),
            "AlumniCredential".to_string(),
        ],
        issuer: "https://example.edu/issuers/565049".to_string(),
        issuance_date: "2014-11-28T12:00:09Z".parse::<DateTime<Utc>>().unwrap(),
        expiration_date: None,
        credential_subject: cred_subject,
        credential_status: None,
        proof: Some(proof),
        terms_of_use: None,
        evidence: None,
    };
    let presentation_proof = Proof {
        type_: "RsaSignature2018".to_string(),
        created: "2018-09-14T21:19:10Z".parse::<DateTime<Utc>>().unwrap(),
        proof_purpose: "authentication".to_string(),
        verification_method: "did:example:ebfeb1f712ebc6f1c276e12ec21#keys-1".to_string(),
        challenge: Some("1f44d55f-f161-4938-a659-f8026467f126".to_string()),
        domain: Some("4jt78h47fh47".to_string()),
        jws: "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..kTCYt5XsITJX1CxPCT8yAV-TVIw5WEuts01mq-pQy7UJiN5mgREEMGlv50aqzpqh4Qq_PbChOMqsLfRoPsnsgxD-WUcX16dUOqV0G_zS245-kronKb78cPktb3rk-BuQy72IFLN25DYuNzVBAh4vGHSrQyHUGlcTwLtjPAnKb78".to_string(),
    };
    let presentation = Presentation {
        context: vec![
            "https://www.w3.org/2018/credentials/v1".to_string(),
            "https://www.w3.org/2018/credentials/examples/v1".to_string(),
        ],
        type_: "VerifiablePresentation".to_string(),
        verifiable_credential: vec![cred],
        proof: Some(presentation_proof),
        holder: None,
    };
    let serialized = serde_json::to_string_pretty(&presentation).unwrap();
    return serialized;
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn ensure_no_runtime_crash_for_example() {
        create_example_credential();
    }
}
